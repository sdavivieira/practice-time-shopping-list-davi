const {src, dest, watch} = require ("gulp");

function styles(){
    return src('styles/variables.scss')
    .pipe(sass({ outputStyle: "compressed"}).on("error", sass.logerror))
    .pipe(dest("dist"));
}
function sentinel (){
    watch("styles/*scss",{ignoreInitial: false} styles);
}
exports.sentinel = sentinel;