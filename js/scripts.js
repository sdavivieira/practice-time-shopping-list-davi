const photoFile = document.getElementById('files')

document.getElementById('send')
.onclick = function () {
   photoFile.click()
}
window.addEventListener('DOMContentLoaded', () =>{
    photoFile.addEventListener('change', ()=> {
        let file = photoFile.files.item(0)
        let reader = new FileReader ()
        reader.readAsDataURL(file)
        reader.onload= function(event){
            let image = document.getElementById('photo-preview')
            image.src = event.target.result
        }
    })

})
var files = document.querySelector('input[name="files"]');

files.addEventListener("change", function(file){
	var input = file.target;
	
	var reader = new FileReader();
    
	reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('image');
      output.src = dataURL;
    };

    reader.readAsDataURL(input.files[0]);
});